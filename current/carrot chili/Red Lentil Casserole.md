

1 cup all-purpose flour *

2 tablespoons dry baking soda *

2 cloves garlic, minced * (about 1/2 cup for a small batch)

15 ounces whole-size whole-wheat wheat flour

2 teaspoon baking powder

1/2 teaspoon salt * (about 4 cups)

1 tablespoon cornstarch (or 1 teaspoon kosher salt for your preference)

Mix all the flour into a large bowl, toss well to combine.

Gently toss a handful of the raw ground Lentil and rice in the oven to bake for 25 to 30 minutes or until golden brown (depending on size of granny). (I used large griddles and if you don't have them you can do a little more work in the oven by mixing together the flour, baking soda and salt, then adding the whole wheat flour and adding to a well made cake pan, or using a piping bag or tin, and bake until the bottom is golden brown and cooked through, about 13 to 15 minutes or until about 3 cups of all-purpose flour are sprinkled on top. (If the top of the cake is too thick, just coat the top with baking soda to avoid it clumping with the bottom.)

When the cake was ready to top, remove the cake from the oven and let cool.

Preheat a small skillet or metal plate to medium (about 15 minutes). Preheat a large skillet or metal plate to medium (about 25 minutes)

Place the cornstarch and baking powder mixture on the stove side where they will start to separate.

Next, combine the flour mixture with the dry baking soda to make dough.

Rinse bread flour and flour mixture on a paper towel or cookie sheet. Spread the mixture on the sides of the pan with the sides down until it is sticky, about 4 to 5 minutes.

Gently fold in the cornstarch and baking powder mixture using a spoon. Once spread, lightly fold in the crust, filling and filling mixture with the remaining flour and the cooled remaining cornstarch.

Top each side of the cake top with the crust and filling mixture. Bake for about 30 to 35 minutes or until golden brown.

Once cooled, place in a bowl with the remaining flour and baking powder mixture (I used my two extra tablespoons or as needed) and bake until golden brown, about 5 to 8 minutes.<|endoftext|>This is the main entry for this week's edition of The Best of