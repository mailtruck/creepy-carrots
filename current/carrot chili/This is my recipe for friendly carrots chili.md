. The flavors were pretty amazing!! Hope people want to try it!!!

I love carrots but have other allergies. And the carrots were a bit on the spicy side while eating those, but the flavors are not as bad as I expected. I found that most of the flavors I was looking for were very good!

Thank goodness I'm a vegan so my veggies came out of the freezer quickly. I would love it if someone would try another variation!

It is delicious. It's not the same as what we see with other veggies we have around the house.

Vegetarian Chili Recipe 2 Tbsp Chili Sauce

2 Cups Soy Sauce

2 Tbsp Vegan Green Chili Chutney Powder

1 1/2 tsp Salt

1 1/2 tsp Ground Fresh Herb Crop

1 1/2 cups Red Lentil Casserole

1 Tbsp Spaghetti/Bacon Cheese

2 Tablespoons Vegan Peppers

1/2 cup Spiced Caramel Mayo

Directions:

1. Place the peppers, green chili and cheese on a baking sheet lined with parchment or silicone, cover and refrigerate for up to 24 hours. When you are done, slice the squash and season to taste.

2. To assemble the noodles, I used a 8" x 11" round pan for this recipe. I used 2-3" x 3" noodles (5-6 lb) so all I have are the leftovers.

3. Preheat the oven to 350 degrees F (150 degrees C). Preheat the oven to 350 degrees F and allow the carrots and chili to cook for about five minutes. When the pasta is fully cooked, remove from heat and let the veggies drain. Pour the spaghetti and pepper mixture over the noodles and season to taste. Top with 3 Tbsp of the chili sauce and a pinch of the fresh herb.

4. In a large saucepan, heat the olive oil over medium high heat. Add the onions and cook for about 20 seconds or until the tender onions are translucent. Let the sauce simmer in the pan for about 4-5 minutes until they start to brown.

5. Add the dried beans and stir the vinegar and vegetable broth until the stew has a thick, glossy consistency. Add the remaining stock and stir it to blend together.

6. Once the sauce is combined, add the rice flour. I did have to add some rice flour to this but it would