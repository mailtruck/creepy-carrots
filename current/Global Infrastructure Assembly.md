 for Global Leadership.<|endoftext|>As most of you might know by now, the 2014-2015 FIFA World Cup is at the beginning of this month in Brazil. There, it's being held in the second round of the 2014 Summer World Cup where the world's top teams are expected to face off against one another. It's an interesting match for you to watch as it means that in your opinion, not everyone is going to see so much action this time as people will most probably have a little more excitement. Well, it might be time to do that and we're here to try so you can see what you've missed out.

We're going to dive into a couple of the most famous players who will be participating in this match and what they're trying to achieve next.

Alex Iwobi

In a nutshell, Iwobi is the son of legendary World Cup winner and FIFA Hall of Famer Iwata (who coached the Brazilian side in 1999), who has been making huge amounts of money this year.

In his short time there he's produced a lot of things, but his greatest asset is his ability to play the game. Iwip has also been a part of many world championships in Brazil and his performances at the 2012 World Cup in Rio could well have given him the title as World Player of the Year.

It's also worth noting that while he's a player that is a bit on the flashy side, if you look at his career stats and his stats against the best and least promising players in the world then he's probably the player you wouldn't expect to see all too often.

There's also a lot of depth in this world. Iwobi is still just 17 years old, but is already making more than $200 million on his current playing career. That's almost $300 million to put his career on track to be in the first team. That makes this tournament more likely to help him put things together but it would still be much more expensive.

Also worth stressing is that his talent is very solid and it appears that he's getting plenty of minutes per game. Considering how the Brazil roster is stacked up, and despite his impressive World Cup experience, we're sure Iwobi will eventually reach the very top of the charts.

Wesley Sneijder

Speaking of those that are on the World Cup roster, we're not going to get into the details as we're a tad bit ahead on the subject here, but if you're feeling a little antsy