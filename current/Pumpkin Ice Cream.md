 (18% ABV)

Nachschapf (20% ABV)

Sage Pudding (20% ABV)

Buckwheats (20% ABV)

Cinnamon Toast (20% ABV)

Pork Nut Cookies (20% ABV)

White Chocolate and Panko Rolls (60/30 Minute, 80/20 Minute)

Cinnamon Bread Spread (60/30 Minute, 70/20 Minute)

Ketchup & Cream Cheese Rolls (60/30 Minute, 60/20 Minute)

Peel and Dice Cake (60/30 Minute, 65/20 Minute)

Pizza (60 minutes, 70 minutes, 130 minutes, 210 minutes)

Googee! These are my favorite desserts. We will share some of them at the end of the day…

Karma Rolls (180 calories)

Kettle Fudge Cookies (180 calories)

Kerry Meringue Cookies (180 calories)

Strawberry and Apple Cinnamon Roll (180 calories)

Pumpkin Ice Cream (180 calories)

Banana Pancakes (180 calories)

Cinnamon and Strawberry Pancakes (180 calories)

Pumpkin Ice Cream (180 calories)

Sugar Cookies (180 calories)

Oatmeal Cookies (180 calories)

Cheese Cakes (180 calories)

Pumpkin Ice Cream (120 calories)

Pumpkin Ice Cream (120 calories)

Pumpkin Ice Cream (120 calories)

Pumpkin Ice Cream (120 calories)

Ketchup and Cream Cheese Rolls (120 calories)

Pumpkin Cookies (120 calories)

Banana Pancakes (120 calories)

Cheese Cakes (120 calories)

Pumpkin Cookies (120 calories)

Oatmeal Cookies (120 calories)

Pumpkin Chocolate Chip Cookies (120 calories)

Pumpkin Ice Cream (120 calories)

Oatmeal Cookies (120 calories)

Pumpkin Ice Cream (120 calories)

The best part is that the cinnamon is just as sweet and rich as the pumpkin! When combined with the cinnamon the flavors are so delicious you will like the taste of them. I love to bake cakes with cinnamon because it's more delicious to me. The cookies would have to be cut into bite sized pieces before they would be rolled. We would