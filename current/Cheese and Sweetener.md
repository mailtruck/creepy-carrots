

Soy Sauce

Spinach and Cilantro

Chicharrones

Chili Paste

Dried Ginger and Onion

Green Peppers

Dice

Gingerbread

Gingerbread with Spiced Tomatoes, Tomato Cilantro, Carrots, Onion, Parsley, and Peppers

Ketchup

Ketchup with Spicy Jalapeño, Pepper Panko, and Green Onion on a Cheeseburger

Rice

Rice with Roasted Granny Smith, Carrots, and Red Pepper Casserole

Red Pepper Casserole with Spicy and Roasted Granny Smith Sauce on a Hot Dog

Salmon

Chili Spiced Potato Soup with Roasted Chicken, Green Beans, and Tofu or Pecan

Fresh Lemon Chili Cheese Dip

Chili Spinach Dip

Black Bean and Cider

Bacon Roast Cheese Dip

Chili Spinach Salad

Brown Rice and Brown Bean Casserole Delish

Chili Salad

Chili Spinach Salad With Cilantro

Makes 6 servings

Serves 4

Makes 4

Makes 4 bowls

Total Time: 4:30-5:30

Ingredients

15 grams unsalted (about 1/4 cup) butter

3 cups (8oz) cashews, stemmed

7 ounces (3/4 lb.) chicken breasts, halved, diced

1 teaspoon ground black pepper, to taste

1 teaspoon ground fresh oregano

1 teaspoon ground sesame oil

1 teaspoon ground cumin

1 teaspoon ground black pepper

1/2 teaspoon ground cloves

1/4 teaspoon crushed ginger root

1/2 teaspoon dried basil

1/2 teaspoon ground parsley

2 1/2 teaspoons ground cumin

1/2 teaspoon ground cayenne

Salt

Directions

Preheat the oven to 400 degrees. Line a 12x13 glass, nonstick skillet with foil. Heat 1/2 cup (15oz) of olive oil in an 8- or 12-quart heavy oil saucepan over medium-high heat. Add the chickpeas (optional), spinach, and celery. Bring to a boil, reduce heat, then reduce heat to low, add chicken thighs, and cook, about 20 minutes, stirring