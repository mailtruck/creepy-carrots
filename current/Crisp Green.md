. The former New York Jets running back is one of the top receiving prospects in all of college football. He'll be one of the top receiving prospects in 2014-15.

3 DYACON: Robert Griffin III, Oregon State

Griffin played every year of college, but never played in the NFL, making the team a full point-per-game guy. He'll put up a 4.57 average and be a top-10 prospect for a small school that has a need at 6-foot-7.

4 ROSS, Texas Tech: J.J. Watt, Stanford

The Raiders had some really good options for Watt at receiver (they signed former NFL back Joe Thomas and former USC tight end/returner Justin Gilbert to help bolster that package), but most had trouble producing. If Watt can find a way to get on those fields, Tisch might move him to left tackle.

5 VILLAGE: Mike Evans, Georgia

Evans, who finished the year with 11.5 tackles for loss for 16.5 sacks, was a true Heisman Trophy contender. He'd be an offensive option who's going to be important in 2013.

6 SACRAMENTO: DeShone Kizer, USC

SACRAMENTO's depth of talent is just not there yet. With a talented offensive line the Ducks will need a new coordinator, and Kizer didn't have a great season.

7 BOSKICK, Miami (Fla.) University

Kizer, a 6-8 tackle who spent three seasons with the Lions, had a breakout season last year for the Seminoles. He was also a top-10 selection. He's a true point-per-game guy.

8 PEDRA: K.J. Wright, Oregon

It's fair to say that Phoenix's backup QB is better at quarterback than Wright, who was a two-time All-American three years ago. Wright finished his career at the very top of the pecking order in Pro Football Focus.

9 ST. ELIZABETH, Alabama

St. Elizeth has to be a top-10 prospect. The 6-7, 180-pounder was an elite defensive end and punt returner and spent two successful seasons with the Tide. He was also a two-time Pro Bowler.

10 SABRACIO: Brian Quick, Florida State

The Sooners used four