 I

My Name is Daisaku. I am an adventurer, adventurer is my name. I are an adventurer. Please tell me.

What's wrong?

Your heart is at an all time low. I must be getting lost. I have to be the last person alive and I am going to save everyone.

"I have to be the last person alive and I am going to save everyone." My voice is deep and gentle.

No way!

You are too stupid for that! Your heart is at an all time low!

Oh. No.

Your head is so cold. You want to go back to earth.

Oh no.

I can feel it. Your body is so cold. I don't know anything. I'm so tired and have no time to sleep.

"But if I do, what are you going to do? What do you say? If the Dragon is dead then I will save that person. I will be fighting him. Even then I am going to try to take the Dragon and not give up.

"No way! I am going to die… What's wrong? You want to take the Dragon so badly?

"You want to kill me. You want not to give up. You wish to kill the Dragon. There is no hope at this point. If the Dragon is alive and you save everyone, you should be alive.

"But, you want to kill me and not save me. You have no confidence. Your heart is going to stop being like this the instant you kill me. I don't want another Dragon being there. I don't want to die."

What if I die.

I cannot wait for that. You needn't go back to me with such nonsense.

I cannot take a stand on the matter again. I want to save everybody in my world. I can only take out this monster from the sky and save everyone alive.

You have no right to be the only one who does that.

How about I stop and let you do that.

Do you still not understand what's going on here?

"Let me go. I said this is for the sake of people and it's just for the sake of money and nothing else. I will die now. I will be gone for the world."

I am so tired and confused.

(The Dragon had stopped moving