 TONS OF GREAT SONGS - PUT OFF THE VELVET - DRAMA EDITION - 3X3/XL - 4X4/5-Piece - $29.99

Buy Now

JARED: THE JARED

JARED is a brand new set that blends classic rock and metal with stunning technology that will get your band to a place you'll feel you're really, really proud to be a part of. This set features a unique, highly detail and extremely unique look, created and crafted by Steve and Ryan from the heart of Detroit. It's a collection of two completely different pieces. The first is a piece created by Ryan from the heart of Detroit, created in the last week of October by Steven. The second is an original piece created by Ryan from Detroit based on a piece created as a fan's take on "JARED".

You'll find an assortment of new items from the set including 2 new album covers, art deco items and more items from The JARED Collection.

JARED brings Steve and Kevin to the big stage where they're at when they make their first live show with a brand new set featuring their favorite acts of all time. And this is when you get the chance to witness our debut as part of JARED! A unique set of two unique tracks that will open your eyes and bring you closer to the story of RZA, "The JARED" and his band, The Man Who Found Me.

All proceeds from JARED go directly to The JARED Foundation and will be donated to The Man Who Found Me Foundation. You can find the complete set HERE!

Buy Now

THE PROMISE OF THE KOREAN BLACKLIST

The Promise of the Kolp is an album that celebrates the black metal scene with three tracks where the band members talk about that great era. And the band goes on to discuss why they came to make it out of obscurity and why black metal was always a special genre.

With no release date in sight these songs and the opportunity to be heard live on the JARED stage is sure to have you raving about it. With this release you'll be able to see the band come together in a new way with their latest album!

And if you're not already excited about the JARED Experience you'll get a very special VIP experience that is a special event (so come on in and grab your tickets