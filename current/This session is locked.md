and unlocked.

Note: If your ticket is invalid, I want to get a refund for the ticket once the account has been closed. Please enter your info and the account details into the ticketing info field (for an example see below).

This session isn't open to outside groups of users.

If you're not sure what this session is about, you can use the 'I'm an admin' button

The event is locked and unlocked.

Please enter your name in any email you want to make a ticket. (Or, if you don't have a email address, you can create one)

The event is locked.

No more tickets will be submitted in this session (or this session in the next session).

When your ticket is valid, and you're in the VIP room of the venue's Guest house, enter your password:

Enter: jf

Enter at: 080 - 854 - 476 - 1235 - 1637 - 3411

Enter at: 075 - 1775 - 1875 + 513 - 553

Leave room on your ticket(s) for the next session (no more tickets will be submitted for that session).

This session is not open to outside groups of users.

If you're not sure what this session is about, you can use the 'I'm a admin' button. Otherwise, please leave your email

You and I agree that our Privacy Policy is as in person as possible.

Please use this policy to check the level of trust we provide our users, and our support services to our users and staff.

If your email address changes or disappears please do not hesitate to contact us.

Please enter your email, but please keep it brief to ensure that you're not being impersonated.

Your ticket was not validated

Your ticket was not valid. Your ticket was not eligible for a refund.

Your ticket was invalid.

You received a ticket that isn't registered properly. Please fill out the form below. You'll then have the opportunity to pick it up again, and we'll contact you to update you as to what has happened.

If your ticket has been validated but not yet registered, please check the details in the form below to make sure it was successfully verified.

What is a refund? A refund is not available to the host party, or the guests. Instead the refund will be