 of pain as his father came up behind him, but the pain was gone. It was like an image of a child being bitten off by a bat or an eagle, the same pain now that he was having. "Please, dad, listen up. It's better to see yourself as somebody who works every day." He had to admit, even though he could still tell it wasn't his own father. His father was a guy who would always take the fight to his son, and he would only let him go back if he was being pushed back enough. Every week, in a normal house, there was one or two kids that went to school in the schoolhouse in the middle of the night, where they usually met their mom and dad or maybe they went to the bathroom. The house was not a great place to spend their days, but it was enough of a place the family wouldn't get involved. The only way to make kids feel happy in the house was to see them in their natural form. "I don't know how I'd do without you, Dad."

"You're not what you used to be. Don't make me feel guilty about doing something like that and you won't. But I have to live with it."

"I have to live with it. And then I have to find other people to take care of it as well."

Dad had no idea how the kid who had made such an important decision would feel even after spending so much time with it. He wouldn't change that now because he was having a hard time finding others who would love him, if only he was going to get it back.

They both wanted to make sure no young lady could ever have this much to lose in the world. They didn't want to take away any of his life because he would just get another job. If the kid who had been in the backseat of the car were to be found, that was all they needed to know. "If that guy was doing the right thing, I'd want him back there. He'd have the support of people, so he'd be able to move forward. If someone's gonna give me that help, that guy is going to get it for me." They both had feelings for their father and hated each other because of it, but now they both had their own ways of coping.

"You'll take over the rest of your life. You'll be there when you need it."

"I'm gonna help