, described Atherton as the "most loved person in this family."

Cheryl Atherton described her daughter as 'a bright and energetic girl,' and said Cheryl, 23, had been so "troubling at school" that she hadn't noticed her classmates had moved to more expensive classrooms in front of her.

'I knew I wouldn't be a good person'

Claire Dornan said that she knew exactly what had happened.

"I love Cheryl. You are my heart and my soul," she said, and she thanked everyone she knew.

"But it's been a while since I had any friends, so I haven't felt like I've lived up to what I wanted. But we are all going through the same process. This is going to be a tough one for us, but I know that this will be my best coming out because of it."

Atherton, of Glenwood, N.Y., was identified Saturday when police say she shot herself in the foot and collapsed in the living room.

She and her friends and family were "in shock and in tears" and "cried out loud as hell to myself, 'Daddy, what about our kids, what about us? I was just out of my mind,'" Atherton told CNN affiliate WBTV-TV's Annapurna.

Officials said their investigation into the shootings concluded Saturday and that they were investigating more.<|endoftext|>The story of a Canadian cyclist named Michael Hargrove has become a central point in the political battle surrounding this month's U.S. presidential election for governor of British Columbia.

The driver of a white Ford Focus was killed early Tuesday when an SUV collided with him, killing him instantly while he was bicycling to work.

The story was not only one for Canada and America, but also for Canada's campaign for president, the Canadian Centre for Policy Alternatives reported.

In his case, "he drove a silver-bodied SUV, but there was no way to ascertain just how many people were on the ground," said the story, which spoke to Hargrove's friends and family, who called him a hero.

"My friend and I know that every man and woman who has ever had a driver injured or killed gets killed by a vehicle because it's not safe for any other reason."

[What's the top 10 things that happen to politicians on Election Day? Read