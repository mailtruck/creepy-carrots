:

Yum!

If you are going to be using a muffin pan as a filling I'd try my best. I found the muffin pan is the best for mixing two ingredients. Make sure they are in very fine shape too! Here's a quick video where I show you how to make a simple muffin pan (not the full muffin pan version!):

I'm really looking forward to making this my next muffins. And I hope you all enjoy this video.


Now, onto my recipe.

How To Make The Sweet & Cool Stuff:

You will need the following ingredients to make a sweet and cool muffin pan:


* 2 large egg

* 4 cups water

* 1/2 cup water

* 1/2 cup milk

* 1 tsp baking powder

* 2 tbsp vanilla extract


* 1/2 cup granulated sugar

* 1 tsp vanilla extract Directions:

Mix the dry ingredients together and set aside.


* When the butter melts the eggs in a food processor over a medium flame.


* Slowly pour the milk and vanilla extract into the butter and make sure the mixture is moist.


* Let it rise until the mixture is light brown and glossy green.


* Let the mixture cool for an hour before adding it to the pan.


* Now add the cream from the same bowl that you use to mix the two ingredients that you used in our previous post:


Now add the flour, salt, baking powder, baking soda and some more water. Then you can mix it up a bit by hand. Then it's time to add the dough. I put some butter on top if you like because I think you would really love it!


* In my muffin pan I also used a spoon to make a thin crust. If you don't have a spoon then don't worry I won't need it!!


* It was nice to have a little extra moisture in the air and it really helps to keep the crust nice and soft. A little sticky may help too too.


* After the bread crumbs get a few cups of bread crumbs and mix them up well. You won't want your crust to stick together. It's a great way to make a beautiful meal without eating it up.


* Add the yeast a little bit if you like because it keeps the butter a little different.


* Add some