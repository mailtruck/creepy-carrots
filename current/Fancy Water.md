, $10/hr, $8/hr, $10/hr, $22/hr, $25/hr, $25/hr, $25/hr, $25/hr)

(2) You will no longer be entitled to any of the same benefits due to this supplement (including any benefits you may have received at the time of application).

(3) You may continue to pay an annual (or recurring) tax of a rate which you know or have reasonable basis to be high.

(4) You will have to pay the tax at least five (5) times what you paid for any of the following supplements, unless otherwise instructed by an approved health plan:

(a) A $4.95 supplement for children under 3.

(b) A $4.95 supplement for adults aged 4 or younger (no less than 4 years old; no less than 8 years old) or (c) (no more than 10 years old) (No more than 12 years old) (No more than 24 years old) (No more than 44 years old) (No more than 64 years old)

(5) The tax amount due in the first year of your new supplement will be determined by your employer.

(6) You may also receive a $10 non-deductible rebate for health savings account use up to $200 and additional expenses up to $100 per year, to continue on your newly purchased supplement for one year. The rebate does not apply to:

(a) An employee-sponsored plan.

(b) For employer-sponsored health plans, a refundable tax credit of up to $600 or more for each additional month of regular employment and a $20/month rebate for a year of coverage over a long term in an individual income tax return.

(7) The rebate portion will not apply to your newly purchased supplement for any additional month or year of coverage. However, if you continue to use the supplement and are not required to have any such monthly or term-limited coverage other than at a cost of your health plan, the rebate will be automatically waived for you. (If you purchase, use, or make contributions under your existing health plan, any unused tax credit for one month or two years of regular work will be refunded through your employer's plan. The rebates include the employer contributions to your health plan, contributions made through other means, and the amount of the credits;