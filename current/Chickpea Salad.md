Salad. I made it with the leftover bacon slaw, and we used the green tomatoes to bake the spinach. The kale was also used to make this recipe as she is the one who said it was a "fantastic" meal with all the spinach in it. We made 6 servings and it was a really good meal. These spinach salads were delicious that night!

I love salads so much I would make them all by myself and make them ahead of time. Sometimes I need my husband to use the blender and add spinach. If I don't know how to do that, I guess I should give him the recipe.

Enjoy!

And don't forget to subscribe to my blog to hear what everyone thinks about cookbooks, giveaways & other great content.<|endoftext|>A US Federal law enforcement official who investigated the alleged murder of a 13-year-old girl in Colorado last year told ABC News this week that President Trump's administration may not have even sought to take a step in his direction when it comes to protecting young people who are targeted by gang crime.

Interested in ? Add as an interest to stay up to date on the latest news, video, and analysis from ABC News. Add Interest

The official, who spoke on the condition of anonymity to give only his position, said that he didn't "think" President Trump had considered seeking legal guidance or action to prevent the 14-year-old being killed.

He said he thought the administration believed the young victim's family might have taken her life in his family home.

"He didn't think he should have done that," the official, who spoke on condition of anonymity, told ABC News. "He didn't talk a lot to his family about that or the family was the only party to this."

ADVERTISEMENT

The official also said that even when it came to the killing of the child outside of college and being the one to put her killer in jail, the president failed to protect those kids because he wasn't willing to commit a crime.

The White House has said the administration and President Trump have discussed ways to try to protect them against more violent crime.

In a July 2016 news conference, Trump made it clear that he was concerned about those young victims, and even mentioned a child's death as a potential threat to his own administration and his administration in February — after the president, Attorney General Jeff Sessions Jefferson (Jeff) Beauregard SessionsGrassley's office says it has received prof