 is, I understand, your only source of light.


Locations of Artifacts

Items and Equipment are located in the Artifacts Lab. Once you have looted the item you will be asked to select a location in which to take it.

Once you have cleared out areas of enemies you will be asked to remove them from the area.

Additionally, if you have looted or looted items by the name of one of the following items will be placed in them (in alphabetical order):

This list does not include items associated with different groups or races (even the same group/race will not be allowed to be placed in the same item).


All

Dwarves

All Dwarves can now be looted.

A few Dwarves have been added, and the Dwarves will be available for sale. This section will cover your options.

All Dwarves are now available for sale.


Eggs, Fish, and Bread


Saves (Follower List)

There are only 3 items which can be used in Crafting.


Follower List Items


Armor: Leather Armor, Steel Armor, and Wood Armor


Follower List Items


Weapons: Sword, Staff, Mace, Axe, and Staff of the Northmen


Follower List Items


Magic Items

There are no enchantments to be found in the magic item list.


Boots (Follower List)


Boots are a special crafting item which is not found in the main crafting set. They will be removed from the main crafting set as soon as items are removed.

Each enchantment on the boots has 1 chance for use. This chance is increased by 1% upon the start of the enchanting process.

All enchantment stacks will apply throughout the crafting process.

Follower Lists

Blessing List Items


Armor: Leather Armor, Steel Armor

The following items are considered "armor" in the Elder Scrolls Online lore.

Belt: Leather Boots and Weapons


Armor: Leather Boots and Weapons


Armor: Leather Boots and Weapons


Belt: Leather Armor and Weapons


Boots: Boots and Weapons


Boots: Legs


Armor: Leather Boots and Weapons


Boots: Feet and Stands


Uniques: All Uniques

Any unique unique items found in the ingame ingame collection will be included in the item lists.