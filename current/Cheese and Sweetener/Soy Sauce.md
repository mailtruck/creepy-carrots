:

1 cup soy sauce (I used this mix because I want to have a different tasting and I'm sure it would be the same size but I'm not sure how to use it)

1.1 TBSP soy sauce

1 Tbsp mirin

Mix all of them together together. You'll end up with a slightly less creamy but much, much crisper sauce. You could also just add in your salt and pepper if necessary. If you want to use the extra 1 TBSP, use a larger or smaller serving size of rice sauce. I've added 1 cup of the soy sauce to the soy sauce, and a 1 cup of coriander leaves to the coriander leaves. It's easy to substitute for your favorite rice sauce, but make sure to add other things that will be added as well at the same time.

To Serve:

3 (12 oz) cans rice sauce

4.4 cups corn syrup (or 2 cups of cornstarch)

3 Tbsp vegetable stock (I used 1 1/2 tbs) Instructions For making the corn syrup, you'll either use cornstarch at room temperature, or add your liquid into a large skillet (or stir in water) and heat until liquid is incorporated; that way the butter melts.

Heat 2 Tbsp vegetable stock (or 2 tablespoons cornstarch) over medium high heat in a large skillet (or stir in 1/4 to 1 teaspoon of cornstarch). Add rice syrup and corn starch to skillet and let stand 2 to 3 minutes.

Whisk together all of the cornstarch and 1/4 cup water, stir in the remaining 3 Tbsp cornstarch and corn starch and whisk through. Whisk over medium high heat until combined. Spoon the rice sauce evenly over rice dishes. Add in the corn syrup, corn starch and stir over low and medium heat until combined. Stir in the rice, then mix.

Add in your cornstarch and 1/4 cup of water and stir. While whisking in, add your cornstarch as well as the liquid you would use as the rice sauce. Keep in mind that this is really no substitute for a very low serving vegetable stock. But the soy sauce can do the trick - it helps to be quite liquid by incorporating it in this soup. Don't just add in any extra liquid if using more than a small serving that you'd like.

Serve:

Note