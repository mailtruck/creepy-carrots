, and brown sugar.

In a large, heavy skillet, melt butter in your pan. Pour flour, egg yolks, sugar, cornstarch, and olive oil over the eggs. Cook until golden, about 20 minutes. Remove from heat and stir into the egg mixture.

Rinse thoroughly with a rubber spatula until it is just coated in the melted butter.

Place the mixture into the refrigerator and cool completely through. It should be completely cooled to room temperature.

Meanwhile, in a small bowl and whisk together butter, egg white, sugar, flour, salt, and cardamom; stir in flour, cornstarch, and egg whites. Set aside. With an electric mixer spray the eggs in a small bowl. Mix in the cornstarch, salt, and cardamom until even.

Once done mixing in the egg mixture, add the remaining flour, sugar, and egg whites to the batter, mixing to combine. Transfer to a greased mixing bowl and place in the freezer for at least 8 hours, or until cooled through and ready to use.

To assemble the cakes:

In a large medium bowl, mix together the honey, vanilla, and vanilla extract. Whisk in the flour and egg whites. Mix until combined.

Toss in the cinnamon sugar, and stir in egg yolks, but you may need a bit of it.

Using a rolling pin, mix the batter for 1 minute, whisking frequently after each addition of the wet ingredients.

After adding the butter to the batter, add in the pumpkin spice topping and cinnamon. Mix into the batter gently until the consistency is well coated. Once the batter is well coated, remove it from the oven and put in the refrigerator for 2 hours for the perfect crunch.

Fry the cake using a wooden spoon until the top is bubbling and golden brown (approximately 30 minutes after baking). Remove and let cool in the pan until it forms a clear, firm cake.

In a large bowl, whisk together the pumpkin, vanilla, and vanilla extract, then combine in the dry ingredients using the roll back of a spoon. Roll out to about two inches, or as far as I use it for that.

Top with the icing sugar and whipped cream.

This post is part of New York Fashionista's 2017/2018 Collection of the best of NYC's best fashion trends. To follow the latest trends and recipes, just follow fashionista