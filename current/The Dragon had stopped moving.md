. The sun was shining, and her body was beginning to shake. Slowly, she felt her heartbeat quicken.

She looked on at the other man to see that he looked like a young woman. He seemed beautiful and well-groomed, almost beautiful from what she thought.

"Hello! How'd your day been?"

The man frowned.

"Well, I just finished the last time I left, I'm just going to have to keep going with the work, as I said, and stay away from the children."

"I-I won't-"

The man raised his hand. "I mean, what are you two doing after breakfast?"

A small smile spread across his face. As though he didn't understand why he didn't know why. Even so, he nodded.

"I've been busy. So here's the other half of my day that seems to give me more information."

"I just came to the Dragon, I have something to do." She paused for effect, a smile forming on her face. The man shook his head.

"No need to get too carried away, it will all be up to you, you can find a bit more information. I shall let you know, what you are doing at the moment."

"I'm going to stay with you, and when you arrive I will leave something over you I think is something you should look into, but I'll do it myself, I'm only going to keep you here for a few days."

"You're still mad?" the woman asked from the kitchen doorway.

"No. It's like my body just went on a rollercoaster, I can feel the pain from my body now. It doesn't feel like much, but I know I could just make this the first day I came to the Dragon a few weeks ago."

She stared into his eyes. "There you are, are you really that mad about leaving? There are all this things about you that really don't matter to you, don't even think about leaving. There's no way you have to know about it and I don't care about you, I don't care about any more, I just want you with me."

The man shook his head, suddenly getting bored. It had taken him awhile to regain the level of curiosity he had back when he was a kid and, after a few months of trying, his mind