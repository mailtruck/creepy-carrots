: Rhapsody of Chaos [TBC, 7/7/2015]

V2: The Rhapsody of Chaos [TBC]

V2: The Rhapsody of Chaos 2 [TBC]


The Rhapsody of Chaos - 5.00 €

V3: The Rhapsody of Chaos [TBC, 7/7/2015]

V3: The Rhapsody of Chaos 2 [TBC]


This was a pretty nice pack though. I'm actually quite glad I hadn't opted for the red or blue but the red was probably more important, which is cool since you can't always go back!


V1: The Rhapsody of Chaos [TBC]

V2: The Rhapsody of Chaos [TBC]


V2: The Rhapsody of Chaos [TBC]


If you have a chance, just don't read that last post. And I'd appreciate it if you did.


The Rhapsody of Chaos - 6.00 €

V3: The Rhapsody of Chaos 2 [TBC]

V3: The Rhapsody of Chaos 2 [TBC]


This wasn't a great pack, but I didn't have much of an issue with it considering my previous ones... and what I didn't like was the fact that it lacked the original Rhapsody version (which had the option to choose the red or blue as opposed to the red or blue), although the red version is better. Overall it was the better one, if not better than the first one.


V2: The Rhapsody of Chaos [TBC]

V2: The Rhapsody of Chaos 2 [TBC]


I think they are the most well-reviewed packs that I've seen in a while

I actually bought this pack from Steam, so if you don't want other good or bad ones, this one should be a pretty decent buy.


The Rhapsody of Chaos - 1.00 €

V3: The Rhapsody of Chaos [TBC, 7/20/2015]

V3: The Rhapsody of Chaos 2 [TBC]


They are great and if you have some cool stuff to pick from this pack, it is probably worth more than this one.


V1: The Rhapsody of Chaos [TBC]