 Trip to San Francisco With the announcement of the San Francisco Game & Sports Show earlier today, the two-time Super Bowl Champion is looking to give San Francisco fans and the people of San Francisco the very best time of his career. The first race at the San Francisco Game & Sports Show will run from 4 to 6 p.m. Sunday night while the second race starts at 9:15 a.m. with a parade through the streets of Mission, San Francisco, on Sunday evenings.

On Sunday, the event will kick off with host host Jimmy Kimmel discussing his new documentary, which comes out Jan. 28 and features interviews with celebrities, sports stars and fans.

"He's very proud of the work he did during his two-year coaching career, and he loves it for that," said Joe DiMaggio , president of San Francisco Sports Entertainment for NFL Network. "The fans were in the stands for the game, they had incredible support. The people in Santa Clara, who really had amazing support, are just thrilled to see how great the coverage was, watching Jerry's win and that show last night. San Francisco fans have to have fun to watch this great man. And he really is that special."

"Jimmy's win, and the great game in general, is something he always wants to tell his fans, but he hasn't been able to do this before," added John Madden, executive vice president of broadcast and marketing for ESPN. "He got it right last time, and I think he's working toward winning this thing this year.

"He's very vocal about his passion for a sports field, which is to have great stadiums in downtown San Francisco, at the stadium, and I think his enthusiasm speaks to his team more than anything else in this sport. He has a wonderful fan base here, one that will take care of their team, and they're extremely passionate in their support of the city and their city," Madden added.

HBO and NBC have announced a new slate of games for the 2014 Super Bowl. Both networks, however, are taking a more pro-active approach to running some of the most anticipated games of the season.

"The 2014 Super Bowl is going on for a few more months and I think he'll do some great things in his life," says ESPN's John Schuhmann , who is looking to capitalize on the excitement he is feeling for the event. "As a lot of those things are coming up, he'll take that and push