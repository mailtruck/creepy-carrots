 are using the Tear of the Earth Dragon and other weapons to attack their leader. She uses the energy of the Earth Dragon as a weapon, sending her flying and forcing the Energi to break down and run after her. They are then teleported off to where the Nulnians are in a battle with a group of Akaviri. The Nulnians quickly kill them off and the Akaviri force are teleported back inside the Tear of the Earth Dragon. The Dragon is eventually teleported away by the Energi and its power is used to bring it safely to Earth.

Kirito confronts Jadom about Shion's prophecy being fulfilled in a scene that also happened during the series. During the final battle against Jadom, the Tear of the Earth Dragon was taken to be the second power source of the Energi, having been created by Shion. The Dragon's power and power is then used as a fuel by Jadom to power up Zephyr.

When Kirito is being chased across the world by the Energi and his Energi companions, the Nulnians arrive and destroy him in preparation for the battle against the Akaviri. They then take him to a hidden location and use the energy of the Earth Dragon to fuel his Energi companion, who turns into a monster that the Nulnians had created to destroy the Akaviri. The Nulnians then use their power to launch a large meteor to blast Shion away with the Sion of the Earth Dragon at the final moments as the Akaviri is being destroyed.

Kirito is initially very angry over the destruction of the Sion that he was tasked with, but he later learns that he is now much more interested in the fight with the other Akaviri. This is when the Energi and Dende become involved, and the Dragon is soon driven out of the World.

After the Battle, the Energi are present at the beginning of the story, including Kirito and the Nulnians when they talk to Shion about the events that happened when the Akaviri destroyed Zephyr.

Trivia

Unlike normal Energi, which is represented by the Dragon in the opening sequence to the movie, the Nulnians' body size and appearance (even though their heads are still in their original forms) is similar to the ones seen in the series. Despite