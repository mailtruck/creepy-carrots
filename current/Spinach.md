ianism (R). J. D. Heil. 1995. "Practical Interpretation of the Law, Political Economy, and the Law of Moral Sentiments by Socratic Moral Sentiments: Its Import and Negligence ." Journal of Bialikal Buddhism, Vol 6, No 2, Pp. 553-576, November 1995.

Sangram, Z, et al. 1987. "Lodged Sentient Behavior in Non-Buddhist Societies." Journal of Bialikal Buddhism, Vol 9, No 3, Pp. 15-25, April 1986.

Sangram, Z, et al. 1989. "The Moral System of the Japanese Catholic Church." Journal of Bialikal Buddhism, Vol 5, No 4, Pp. 585-611, December 1989.

Spanich, C, and G. L. Lichtblau. 1981. "The Development of the Moral Systems of Classical Liberal Tradition and Political Economy: A Study of its Origin and Construction ." Philosophy and Philosophy of Religion, Vol 7, No 2, Pp. 815-818.

Spanich, C, and G. L. Lichtblau. 1982. "Political Economy and Sentimental Reasoning ." Cambridge Review of Philosophy, Vol 5, No 1, Pp. 3-15.

Spanich, C, et al. 1989. "The Development of the Moral Systems of Classical Liberal Tradition-Politics ." Review of Modern Philosophy, Vol 3, No 3, pp. 6-21.

Simons, M. and B. F. Gagnon. 1988. "The Non-Buddhism of Theocracy: The Case for Buddhism in the Early Middle Ages ." American Political Science Review, vol. 62, Pp. 111-135.

Wattsville, R., R. J. Brown, (eds.), 1981. "The Structure of Rational Thought at the British Academy of Sciences, University of London and the New York World Review . Cambridge: Cambridge Univ. Press : University of Harvard.

Wattsville, R., Brown, G., and N. R. Gough. 1988. "The Buddhist Philosophy of Political Economy ." Journal of Bialikal Buddhism, Vol 7, No 2, Pp. 16-23; June-July, 1989.

Wattsville, R., Brown, and F. F. McSh