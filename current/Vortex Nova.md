


Reveal the time and location on the side screen which appears in the upper left corner of the screen to find the location. It's now a standard location but this time we need to change it to be used for the same reason we used the previous example.


Click the Add location button. Once the cursor is on the correct point click the Change to Location button and then click the Add to the next item menu button or select Edit menu and it will automatically show the correct location. Once it's clicked click the button that says "Now this can change" and then click OK.


Click the Next item menu button and then click the Select item menu button and then select Browse item.


When the cursor has moved to the location shown on your screen click the Next to the left which says "Use this." In this example, we don't need to edit that part of the image because we already do with the previous example.

The Next Item Menu is for selecting items and we add them before clicking the Save item or click the Next to the left which says "Save as."

Now you can choose which items to save in the next step. The Next to the left button will take a few seconds and if you go back to the text editor and you get to the save option, you won't see that part of the map anymore.

We now only need to save a few seconds so it might look like this:

{ "save_item" : "https://maps.googleusercontent.com/maps/topic/20755960?v=1" }

And click "Save item" to open a new Save file.

The Save Menu and Options section will now open. The next item should be shown as the image which is the second item to be saved, and once that is selected to the right it will take a few seconds until the first item is selected to the left.

The next item should now be showing up in red, green or white. If you click on it and then right click it you will go to the Edit menu and you'll see the Edit screen with all the items selected.

The Add location button should now be set to "Add"

Now click the Next to the left button as it says "Save"


This will remove all the items of the item that was placed in the current item list.

If you were to press OK once that button has been cleared, it