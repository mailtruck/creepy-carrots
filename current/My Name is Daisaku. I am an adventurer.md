 from the Kensei-shikagami area. I can be seen as a pretty typical Japanese girl and I have a lot of luck, but even I have trouble finding people willing to give up their lives on me. I have a large family and my father worked as a cook and I am really lucky as well. As an adventurer I really do have that luck but I feel I am a little more timid. I'm pretty scared because I know everything is coming in the wrong direction. I'm quite young as well so I'm afraid of heights. But I have this plan for myself which is to find people who won't let me in and if I say I do I'll try my hardest to find a way to save others. I will get out of this predicament so please help me out.

Chapter 15: "It's not bad, but..." - Anya (from chapter 1)

"There is a boy whose name was Daisaku who has no problems at all." Subaru asked as he passed by the scene where everyone was gathered. He also got back into the same situation as before. He thought about who that was. No matter how hard he tried, what could they do before they had to go on with their lives.

Anya came out with some food as well. When her mother, Shizune, brought back the items, their parents quickly stopped eating with everyone looking suspicious. She looked at Subaru after the meal with a sad expression on her face.

"We have no choice. They are all the same." Anya said while she had her food. After that she had cleaned up the meals.

Subaru started eating at his meal because he had a feeling, but he couldn't help but be surprised when after his food, a red-haired man came out from behind. The reason this might be an attack was because of a strange aura. The man looked scary though and when he talked, he began to talk with him.

"He's here to help me, but we need to get home to a bit early. I have to go outside to find some things." Subaru said while making some breakfast. When they finished, he went to sit by Subaru in the living room and sat up. He thought to himself that he is still alive and would come out by his bedside every night.

"There is no way I could help you." Shizune said while she told him that.

"Yeah, just try