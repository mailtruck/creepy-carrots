make it an interesting story," he says. "But also to make it stand out to more players, to add to it a bit something. Not at all a one-on-one and just about any kind of game."

The video, which was taken by a bystander, follows Ketchum as the gang, including his brother, prepare for a high school game. They're heading to a school on the east side of town, waiting for the high school bus they heard a few minutes ago. They have to cross into the school without permission from the principal, who tells them not to even come in from the school gate.

But when they turn around and walk back, it's too late. The bus turns off and they fall out; there's a small fight as the group turns around to face them. The man who made the video calls the school and tells the gang to head back to the bus. He does, and after several hours at the school, they eventually run into a second man.

"He has a big knife in the back," Ketchum explains, "and he's been carrying my brother in a stretcher."

When his brother reaches out for his camera and asks the man where he is, the man tells him to get on the buses.

"He will kill you. Then you can go out on the street," Ketchum declares as he looks at the dead man.

The man, he says, "was in a van. He was just carrying my friend, and the van had taken off from the gate. So we looked and he didn't go. So we drove down the street, and I could see him, and I saw him in the van. There was probably about a minute he didn't even go down the street."

Ketchum also tells the gang he had a pistol in his hand.

"It's not that I would kill him, it would just be my brother," his brother says, adding that this is not the first time this has happened to him.

"We're all family, and we're friends," Ketchum explains, "You never know what could happen without somebody's name on it. It makes me really upset that we can get shot just because he didn't go away. It's not like this kid is some dumb, evil man who is going to kill you."

Ketchum wants to see those who got shot go to heaven, too,