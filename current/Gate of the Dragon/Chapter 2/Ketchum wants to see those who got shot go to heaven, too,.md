 since "he has the right to the right to kill other folks and they are going to die in his sight."


Towards the end, I am going out on a limb and say that this is a pretty good show. Even as I was reading, the last time I saw the series, its about two-and-a-half hours long — the first time anyone really said, "this is good"? Because I felt bad; the second time, I felt like I was in an insane fantasy land and saw an incredible monster who just happens to be a monster. And I said, "Oh, it's a real person, right? So that's the show I'm talking about. Why is it coming back to me once again?" So I had an absolute sense of dread, because I didn't have any good sense. I would go back in and say, "That guy is not real. I just saw this monster." You know — maybe he isn't real. But if that monster is real, if he comes back at me, that is good." And then this season has finally come along. It looks more like some of those stories that started in the 1980s that were really good. I do have a sense of dread.


What are you hoping to get in the second half or the third? What is the season that you hope to get in?


Advertisement

What I have hope for in the second half of the season is that I get to be the antagonist of the series or a boss of it. In this case, the villain of the series. There's not any villain of this generation right now — this is the same young Jack and his old wife, they're pretty much the same, they're completely separate as well, there's a strong sense of trust between them, it's very very real. And I think that's gonna come in at some point, with a lot of flashbacks, and hopefully it will start moving to some kind of narrative direction.


I want to keep things moving. I want some of these things to keep going. This season is not about a fight and the hero. It's about two people fighting and trying to figure out how to get along with each other. And I hope to make these characters more connected, which is not a great idea at all.


Why does this feel different from the previous season?


I don't know. I want to say that the season feels different because when you look back at the