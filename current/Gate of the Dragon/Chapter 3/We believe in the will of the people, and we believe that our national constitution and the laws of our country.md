 need to be put into force as soon as possible, because no one should ever seek to take advantage of that power to take advantage of the state's inherent right to police or to seize.

We also believe that citizens' civil liberties should be safeguarded at home at a time when the security of Americans depends on their loyalty to the American government.

To the extent possible and consistent with our constitutional responsibilities to protect our constitutional freedoms, we will, but we will keep in mind the core principles of our constitution, the Constitution's First Amendment guarantee, and our public officials' responsibility to protect our borders when they see that some people here are crossing borders and others are entering.

Our national executive and legislative branches have the resources, the power to enforce and enforce our laws, and they have the authority to stop foreign fighters, drug traffickers, and terrorists from entering our country. We will protect our borders when America wants to come into our country.

But we will also be prepared to stand with people here on the grounds of law, not just the Constitution, who have a right to protect themselves, or the security of their friends in any way they choose, and we will vigorously oppose all forms of political interference in the national affairs of this Nation.

We will be prepared to stand with American servicemen and women who are fighting against the radical forces who are determined to enslave our Nation to a state of perpetual war if it comes to our shores.

We will stand on the sidelines of a national debate over how to prevent terrorist attacks.

We will support an expansion of law enforcement powers to include the seizure of firearms in their investigations and prosecution of crimes, and we will push the government to increase the time that it takes to review an arrest without due process of law.

We will support a plan to expand our police power, to make it possible and safer to arrest, and to detain persons suspected of terrorizing foreign citizens, which makes our Nation one of the most dangerous countries on earth.

And we will continue to stand with our citizens against those who threaten our Constitution, our Constitution's freedom, and our national sovereignty against those who threaten our liberties and our basic freedoms.

And we will continue to stand with our citizens against those who spread hatred against American citizens abroad.

And when it comes to America's relationship with its citizens, we will not let those who seek to undermine our Constitution destroy our country's security.

We will remain vigilant abroad, and at home, against