 Tattoo is dead, though, and the Dragon is fighting against the Dark Triad. He will be resurrected by Professor X.

Once again, Mr. Freeze has been found out, and the two meet up with the students on the team for a talk between the students.

In the next scene, Mr. Freeze is talking to the Professor, and Professor Ollivander comes to his assistance.

In the next scene, Mr. Freeze is sitting under the school building near the lake and in a large room where he has a sword and a bag of sweets in hand, looking out, then he has a very large sword and a small bag of sweets with him. In the next scene, as he is talking to him, there is a small hole in the ground in front of him.

Mr. Freeze appears in a few more scenes like these before he died. He is seen standing in the air.

In another flashback with Mr. Freeze, when he first appears as a masked man in one scene, he and the professor discuss how the professor got the Dragon Tattoo and how he used it to kill Alva.

In the next scene, Mr. Freeze goes into one of the rooms to reveal to Mr. Freeze's team the "Secret Garden of Mr. Freeze's Dragon Tattoo." Mr. Freeze states he has been to the secret Garden, but instead of talking to his mentor, it was revealed to be a cave. In the next scene, Mr. Freeze's team's back and forth looks very close-up.

In the next scene Mr. Freeze reveals his plan. Once the Professor tells Mr. Freeze that the Dragon Tattoo was made specifically for Professor X, Mr. Freeze gives the dragon tattoos to the Professor.

In the next scene, Mr. and Miss Piggy are on a small school tour of the cave. The Professor and Dr. Horrible come and explain the reason for the tunnel hole. After all the words used to explain it, Mr. Horrible comes out of the cave and says "What happened to their parents in that cave?" Mr. Horrible says he heard about this cave and the tunnel hole until someone gave him a map and told him they heard what they're doing, but he didn't want to leave the cave and he doesn't know what happened there, but they did, and he left. And just then the Professor comes out from the cave and says "Who knew we had to leave it behind?" Mr