 some sort of retail expansion (or even a "buy now and go" deal) before its Kindle-sized market, it's going to do something else: it wants to make sure people who own Amazon's digital digital business know that its physical-computers will be able to compete with Amazon's e-books just fine.

How it wants to compete with Amazon

At this point, it's pretty clear that Amazon has been trying to expand its e-book market ever since the company launched its last e-book, Amazon Kindle. Now with the introduction of the Kindle 3.0, what has Amazon changed since the last 3.0? You'll notice that, by the end of this year, the e-book market will be much more accessible to more people. That's because Amazon's e-book sales will increase, the business will grow, and they'll be able to compete on pricing and margins.

Amazon has put more emphasis on marketing the Kindle 3.0 rather than on how to sell it, so you'll see that Amazon has a much more efficient website to sell its e-books, better e-mail apps, a much better e-book store that can keep track of your order, and its own cloud storage that offers a cloud-based version of the software.

What's different from this new approach to Amazon products is that now you only pay for the hardware and services, so in other words, you can only buy one model of your Kindle when you use Amazon's own, comparable hardware, and then have to buy the one that's online before you go to town. But, what the Kindle 3.0 does is it makes it much easier for someone else to buy the 3.0, because the 3.0 is the physical physical computer product and it's always there to ensure you can get the 3.0.

When Amazon launches this year with the Kindle 6, you'll see that it's also coming with the new and improved Kindle Fire TV, a very, very big surprise. They also announced that Amazon has bought the company's second big seller, Netflix, into a joint venture with Google.

So how much better will this new Amazon competitor make the e-book experience for people who use its physical computers and e-book apps? Well...well...that's actually not a question until someone's reading the review of one of their own books. Because of this, the Kindle Fire TV will only be available as an online purchase on Amazon's