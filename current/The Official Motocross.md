Rules for the Day


I will try to take the time to explain every element of the rule book. The purpose of reading or preparing the rules is to help newbies get started. This page contains all of the information you need to get started at the beginner's and intermediate levels.


If you're new to the rules, check out my guides on what you need to know to become the world's fastest racers. Some tips about how to read the rules, read about "good racing skills", watch YouTube videos or check out the best rules tutorials of all time.


So let's get started!


All-Round Road

In The Dirt Dirt

The Race Is On

I'll show you what comes when a car goes straight for a corner just before the car gets to the pit stop. We'll talk about how the car goes down the hill at the time of the race, the lap markers, how to do the corner, and more. I'll explain what we do at the start of every race, what the different tires are and why it makes such a big difference when you're running fast on a new track. Plus, I'll try to put everyone into an attitude that's easy to learn, so you don't even have to follow rules anymore.


There's a lot more. We'll make sure you're familiar, but don't worry about getting ready with the rule book, you'll get all the information that needs to be in the book.


So get ready. I have you covered.


And we're halfway through the start!

If you enjoyed this guide, you may want to check out the other parts of this guide. For further tips on what it really takes to know how to run faster than you're used to, check out our videos.


Get ready. I will show you what comes when a car goes straight for a corner just before the car gets to the pit stop.


It's the one way to start your daily game.


The All-Round Road


I will show you what comes when a car goes straight for a corner just before the car gets to the pit stop. We'll take you through how to read the rules and what will make the track go faster. I'll explain how the car goes down the hill at the end of the straight with three laps left at the beginning, how to do the lap marker, how to read to your friends, and a whole host of other topics