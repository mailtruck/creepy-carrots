, and was an evil sorcerer.

After discovering that one of his students had been abducted by a group of the Death Elves, the Nulnian sorceress returned to the main part of Nulnia with her group of friends, and found a place to live and gather food. She also gave birth to a child in an orphanage, and that child turned out to be a very good vampire girl.

After completing her mission, the first of the Nulnians to see Rin, who is still alive, went on to become Rohan's First Lady.

A year later, the Nulnian sorceress Nulnian was taken to Taurir, where she was taken prisoner by the Lord of War and his son, Rohan. She fought the other Nulnian sorceress, Rohan, for his life, until Rohan and his family escaped Taurir.

Rohan, a vampire, survived the battle to save his people. After the war, she turned to the Lord of War for aid. It was during this time that he saved him, and he brought a young girl to him, the Lady Elisabeth, who was the Lady Elise.

On her first mission to the city of Dún on her journey to retrieve the girl, Rin was killed by Rohan, but she had no problem with him being there, not knowing who he was.

After an attempt to kill the vampire was made in the name of Taurir, he attempted to kill a vampire in the name of Erebus, but it did not work.

During a failed attempt to kill Dúnedain Aran by trying to bring out Gavron, Rin turned to Erebus and asked him if he could help with the battle. Upon hearing that Erebus was dying, Rin returned to the Lord of War, and he took her along with him.

After that, she was kidnapped by Lord of War Erebus while on his way to Rohan, and gave birth to a baby girl on her way to the Lord. Rin was born with two sets of teeth, but Erebus managed to retrieve her and free her, and the two started to fight. Shortly afterwards, Gavron was killed to prevent Gavron from taking another daughter with the intention of taking more children, which Erebus didn't want to do either.

In order to protect Sion, the Nul