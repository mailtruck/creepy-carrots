 with the "unfavorable weather conditions" and said the incident would not affect any national security or intelligence community missions or operations.

"The U.S. and its allies are prepared and eager to work closely with those interested in a peaceful resolution of the Syria conflict. This cooperation is consistent with our longstanding efforts to defeat the Islamic State of Iraq and Syria," the statement from the White House said.

Trump was scheduled to meet with Putin on Thursday afternoon at Trump Tower in New York. On Thursday evening, Trump spoke to Putin on several other occasions. The president has also been scheduled to meet with Putin and French President Francois Hollande on Friday.

Trump, the president of the world leaders' gathering of world leaders, did not attend a meeting between the Russian president and the French leader on Thursday.<|endoftext|>LAS VEGAS—Sporting KC and San Jose Earthquakes FC's first preseason match Thursday will be the home of The New Beginning, an organization founded in 2009 by Earthquakes owner Carl Robinson that specializes in the homegrown players it hires to compete at the highest level of professional soccer.

The New Beginnings will be the home of the New Beginning Foundation, a nonprofit organization created to raise awareness of the players' health and wellness. For as much as $5,000, members will receive a commemorative shirt, two tickets to the match and an exclusive access to the new home, including a chance to catch a special match and to meet up for the match. The match is set for 3 p.m. at KC United SC on July 1 and can be streamed online at: https://thenewbeginnings.org/event/4-june-2013/thenewbeginnings/

There will be a Q & A session on Tuesday at 8 p.m. with some of the players, who were invited to take questions, attend the match, play and practice. Then the match kicks off at 6 p.m. at KC United SC, with the home opener featuring San Diego State FC, who will travel to San Diego on Oct. 4.

"This is very special moment for us," said Quakes head coach Jesse Marsch. "We're very fortunate with this match. San Diego State is a top-tier, top-tier team, so there's no reason we couldn't bring it to the soccer field. It's very special for us and I'm really excited about it and how we get some help out there. We're going to have to wait