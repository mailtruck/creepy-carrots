icky.

When did everyone know this was such a terrible idea?

"You'd rather just die than watch a movie?" one of the girls murmured.

"Oh. So she knew it would make me feel better about something even more depressing. She didn't like what was happening. She didn't like me getting involved in some weird romantic relationship with an idiot. I didn't like how she was so naive to think I'd like it. I didn't like being with someone who looked all messed up when I had no idea what she was talking about. That's not to say that's a coincidence. I just didn't feel as if that was what people should be doing."

Oh, but that wasn't what she meant. The girl was just trying to make her feel better that she didn't like or feel like it was a possibility. She didn't feel like the world had any idea about her life yet.

"I can see that. But the way she's trying to act this way is that she can't stop it. She doesn't want to see me lose sight of it because she knows that this is just some bullshit that I can't live with. She can't just think for herself. She has no idea what is about to happen and isn't sure what to worry about. She's just trying to act that way for too long. I hope you can do that."

"Oh," her voice rose up. "I am a goddess."

"And a goddess!"

"I'll show you. Tell you what."

"No, don't."

"Come on."

A minute later, and the moment was nearly over, and they were together. Her face turned red, and she said, "I want to try and stop this whole thing. Don't give me so much of a reason. I'm just glad I don't have to see her for the whole day."

"I'll tell you what. If you want to try it, come by. If you don't want to come by, don't come by. But if you want to help me, then I'm willing to try it."

She turned around and leaned over towards the corner of the table, pressing her head against the side of her face. "It's not going to work that way. I'm not going to get your hopes up just because I have the strength to, but the more you believe I could