 off your rating, the app will only pick it up. In other words, all you need to do is tell us which of your favorite videos should we make the review so that we can find it, or do we give you free reviews. You know, they always are.<|endoftext|>The former Republican governor of Iowa said Sunday on ABC's "Good Morning America" that he is considering running for president if Donald Trump wins the Republican nomination to replace former President Barack Obama.

In a "Ask Me Anything" about his life, Mark "The Mark" Perry, a retired Iowa state government official, asked Perry about his plans to retire if Trump won the Republican nomination.

"I think it's time to consider. That's a great idea. But I think it doesn't get done without a big plan," Perry said, noting Donald Trump has "made tremendous progress on that path," but "at this stage … he won't go back to the White House and you don't see a lot of the work done on that ground. It would be foolish for people to take his place. I know that a lot of people have asked this question but it's time for the president to make a decision on whether he ought to run."

Trump has faced criticism for failing to win the Republican nomination as well as being unable to meet expectations, making his campaign a public relations disaster that had political potential.

Asked if he had been contemplating running on Trump after he lost the general election to Democrat Hillary Clinton, Perry said, "I just know the answer to that question. We can't just take it day by day. We need to have real leadership. We need to be really committed to the plan and not just an off-the-plan thing for people to come to Iowa with little ideas and just say, 'I'm going to put forward a real plan that's going to win.'"

The news came just hours after the New York Times reported that Trump would be open to hiring an ethics lawyer in any case against his presidential campaign. Trump's campaign has said it would not accept such a claim.

The remarks come just days before Republican presidential candidate Donald Trump leaves his home in Palm Beach, Florida, for an event with Gov. Terry Branstad. Trump's campaign issued a statement on Friday, saying, "Mr. Trump is committed to our campaign's stated and proven commitment to transparency about his business dealings."

"I have been very pleased with the outcome of the election, and for the record,