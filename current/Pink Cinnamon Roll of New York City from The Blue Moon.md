, an anthology of essays from writers including Peter L. James, Dan Auerbach, and Robert H. Wills. The book begins by providing some of the background to Auerbach's career, including what we've described as the 'first really clear glimpse [of his] post-punk roots'.

The original manuscript of The Blue Moon was written in 2003 by Peter Wills. It's quite a long novel, but in the end it takes its cues from James' work in a new way, exploring more of James' art and aesthetics – what he sees as an interplay between literary culture and art history as well as his own experiences as an educator.

The first draft manuscript of John Bowers's The Blue Moon was a two-page outline of what followed. James' first draft of The Blue Moon is described as 'a rough sketch of something that I had never imagined when I started writing this book'. Auerbach later wrote a draft of his own text in which he says 'the book will be published in a format which will appeal to young writers who have just started reading the work' and that it will be the first time he'll write in a published book.

The book comes to an end with The Blue Moon: a book featuring The Times magazine's debut feature, a preview of The Black and White (2013) and the short fiction anthology Unraveling the Empire: a short novel collection in the series.

The first collection of The Blue Moon will follow on from the book's title and be released in December 2015, followed by a short story anthology, The Black, White and Unraveling the Empire, which will be published in March of 2017.

Auerbach said: 'I think it reflects a lot of what we were thinking of when we started looking at our book and we thought: "Well, it's really a nice story for the time – what should it be like to be in New York City?", which was interesting stuff. The first collection in the series will be a mix of short fiction and novel.

'We are hoping this will be a book that will resonate in its own way and that will reflect New York City itself more than the city itself.

'I think it will take a lot more to say, and a lot more that will work, that New York City as my own city is my own world and also my own vision. At the very least it should have a more personal appeal to me in its own way